<?php

namespace App;

use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Dispatcher;
use Phroute\Phroute\Exception;

class TTFApp
{
    public function start()
    {
        return $this->checkRoute();
    }

    public function version()
    {
        return "TTF Assigment Version 1.0.0.";
    }

    private function checkRoute()
    {
        $routes = new RouteCollector();

        $path = __DIR__.'/Routes';
        $files = array_diff(scandir($path), array('.', '..'));

        foreach ($files as $file){
            $aux_file = __DIR__."/Routes/$file";

            if (!file_exists($aux_file)){
                $msg = "Partial route [{$aux_file}] not found.";
                throw new FileNotFoundException($msg);
            }
            require $aux_file;
        }

        $dispatcher = new Dispatcher($routes->getData());
        try {
            $response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        }
        catch (Exception\HttpRouteNotFoundException $e) {
            echo "<pre>{$e}</pre>";
            die();
        }
        catch (Exception\HttpMethodNotAllowedException $e) {
            echo "<pre>{$e}</pre>";
            die();
        }

        return $response;
    }

}