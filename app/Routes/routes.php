<?php

$routes->get('/', function (){
    return "Initial Page";
});

$routes->get('/version', function (){
    return \App\TTFApp::version();
});

$routes->group(['prefix' => 'ttf'], function() use ($routes) {
    $routes->get('base/{a}?/{b}?/{c}?/{d:i}?/{e:i}?/{f:i}?', ['App\Controllers\TTFController','getBase']);
    $routes->get('sp1/{a}?/{b}?/{c}?/{d:i}?/{e:i}?/{f:i}?', ['App\Controllers\TTFController','getSP1']);
    $routes->get('sp2/{a}?/{b}?/{c}?/{d:i}?/{e:i}?/{f:i}?', ['App\Controllers\TTFController','getSP2']);
});