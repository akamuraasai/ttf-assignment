<?php

namespace App\Controllers;

class TTFController
{
    protected $a;
    protected $b;
    protected $c;
    protected $d;
    protected $e;
    protected $f;

    private function setUp($a, $b, $c, $d, $e, $f)
    {
        if (!$this->checkInputs($a, $b, $c, $d, $e, $f))
            return false;

        $this->inputsValidate($a, $b, $c, $d, $e, $f);
        return true;
    }

    public function getBase($a = null, $b = null, $c = null, $d = null, $e = null, $f = null)
    {
        if (!$this->setUp($a, $b, $c, $d, $e, $f)) return "Invalid input given.";
        $response = $this->calculate_base();
        return json_encode($response);
    }

    public function getSP1($a = null, $b = null, $c = null, $d = null, $e = null, $f = null)
    {
        if (!$this->setUp($a, $b, $c, $d, $e, $f)) return "Invalid input given.";
        $response = $this->calculate_sp1();
        return json_encode($response);
    }

    public function getSP2($a = null, $b = null, $c = null, $d = null, $e = null, $f = null)
    {
        if (!$this->setUp($a, $b, $c, $d, $e, $f)) return "Invalid input given.";
        $response = $this->calculate_sp2();
        return json_encode($response);
    }

    public function getProperties()
    {
        return [$this->a, $this->b, $this->c,
            $this->d, $this->e, $this->f];
    }

    private function checkInputs($a, $b, $c, $d, $e, $f)
    {
        if ($a === null || $b === null || $c === null ||
            $d === null || $e === null || $f === null)
            return false;

        return true;
    }

    private function inputsValidate($a, $b, $c, $d, $e, $f)
    {
        $this->a = filter_var($a, FILTER_VALIDATE_BOOLEAN);
        $this->b = filter_var($b, FILTER_VALIDATE_BOOLEAN);
        $this->c = filter_var($c, FILTER_VALIDATE_BOOLEAN);

        $this->d = intval($d);
        $this->e = intval($e);
        $this->f = intval($f);
    }

    private function calculate_base()
    {
        if ($this->a && $this->b && !$this->c)
            return ['X' => 'S', 'Y' => $this->d + ($this->d * $this->e / 100)];

        else if ($this->a && $this->b && $this->c)
            return ['X' => 'R', 'Y' => $this->d + ($this->d * ($this->e - $this->f) / 100)];

        else if (!$this->a && $this->b && $this->c)
            return ['X' => 'T', 'Y' => $this->d - ($this->d * $this->e / 100)];

        else
            return ['X' => 'other', 'Y' => 'error'];

    }

    private function calculate_sp1()
    {
        if ($this->a && $this->b && !$this->c)
            return ['X' => 'S', 'Y' => $this->d + ($this->d * $this->e / 100)];

        else if ($this->a && $this->b && $this->c)
            return ['X' => 'R', 'Y' => 2 * $this->d + ($this->d * $this->e / 100)];

        else if (!$this->a && $this->b && $this->c)
            return ['X' => 'T', 'Y' => $this->d - ($this->d * $this->e / 100)];

        else
            return ['X' => 'other', 'Y' => 'error'];

    }

    private function calculate_sp2()
    {
        if (($this->a && $this->b && !$this->c) || (!$this->a && $this->b && $this->c))
            return ['X' => 'T', 'Y' => $this->d - ($this->d * $this->e / 100)];

        else if ($this->a && $this->b && $this->c)
            return ['X' => 'R', 'Y' => $this->d + ($this->d * ($this->e - $this->f) / 100)];

        else if ($this->a && !$this->b && $this->c)
            return ['X' => 'S', 'Y' => $this->f + $this->d + ($this->d * $this->e / 100)];

        else
            return ['X' => 'other', 'Y' => 'error'];

    }
}