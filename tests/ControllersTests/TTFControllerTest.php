<?php

namespace Tests\ControllersTests;

use PHPUnit_Framework_TestCase;
use App\Controllers\TTFController;

class TTFControllerTest extends PHPUnit_Framework_TestCase
{
    protected $controller;

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function setUp()
    {
        parent::setUp();

        $this->controller = new TTFController();
    }

    public function test_of_checking_the_inputs()
    {
        $result1 = $this->invokeMethod($this->controller, 'checkInputs', array(true, true, true, 0, 0, 0));
        $result2 = $this->invokeMethod($this->controller, 'checkInputs', array(null, true, true, 0, 0, 0));
        $result3 = $this->invokeMethod($this->controller, 'checkInputs', array(true, null, true, 0, 0, 0));
        $result4 = $this->invokeMethod($this->controller, 'checkInputs', array(true, true, null, 0, 0, 0));
        $result5 = $this->invokeMethod($this->controller, 'checkInputs', array(true, true, true, null, 0, 0));
        $result6 = $this->invokeMethod($this->controller, 'checkInputs', array(true, true, true, 0, null, 0));
        $result7 = $this->invokeMethod($this->controller, 'checkInputs', array(true, true, true, 0, 0, null));
        $result8 = $this->invokeMethod($this->controller, 'checkInputs', array(null, null, null, null, null, null));

        $this->assertEquals(true, $result1);
        $this->assertEquals(false, $result2);
        $this->assertEquals(false, $result3);
        $this->assertEquals(false, $result4);
        $this->assertEquals(false, $result5);
        $this->assertEquals(false, $result6);
        $this->assertEquals(false, $result7);
        $this->assertEquals(false, $result8);
    }

    public function test_of_validating_the_inputs_and_getting_properties()
    {
        $this->invokeMethod($this->controller, 'inputsValidate', array(true, true, true, 0, 0, 0));
        $result1 = $this->controller->getProperties();

        $this->invokeMethod($this->controller, 'inputsValidate', array(false, false, false, -12, -12, -12));
        $result2 = $this->controller->getProperties();

        $this->invokeMethod($this->controller, 'inputsValidate', array('true', 'true', 'true', '0', '0', '0'));
        $result3 = $this->controller->getProperties();

        $this->invokeMethod($this->controller, 'inputsValidate', array('false', 'false', 'false', '-12', '-12', '-12'));
        $result4 = $this->controller->getProperties();

        $this->invokeMethod($this->controller, 'inputsValidate', array(true, null, 'true', 0, -10, '20'));
        $result5 = $this->controller->getProperties();

        $this->assertEquals([true, true, true, 0, 0, 0], $result1);
        $this->assertEquals([false, false, false, -12, -12, -12], $result2);
        $this->assertEquals([true, true, true, 0, 0, 0], $result3);
        $this->assertEquals([false, false, false, -12, -12, -12], $result4);
        $this->assertEquals([true, false, true, 0, -10, 20], $result5);
    }

    public function test_of_calculating_the_base()
    {
        $this->invokeMethod($this->controller, 'inputsValidate', array(true, true, false, 10, 20, 30));
        $result1 = $this->invokeMethod($this->controller, 'calculate_base', array());

        $this->invokeMethod($this->controller, 'inputsValidate', array(true, true, true, 10, 20, 30));
        $result2 = $this->invokeMethod($this->controller, 'calculate_base', array());

        $this->invokeMethod($this->controller, 'inputsValidate', array(false, true, true, 10, 20, 30));
        $result3 = $this->invokeMethod($this->controller, 'calculate_base', array());

        $this->invokeMethod($this->controller, 'inputsValidate', array(false, true, false, 10, 20, 30));
        $result4 = $this->invokeMethod($this->controller, 'calculate_base', array());

        $this->assertEquals(['X' => 'S', 'Y' => 12], $result1);
        $this->assertEquals(['X' => 'R', 'Y' => 9], $result2);
        $this->assertEquals(['X' => 'T', 'Y' => 8], $result3);
        $this->assertEquals(['X' => 'other', 'Y' => 'error'], $result4);
    }

    public function test_of_calculating_the_sp1()
    {
        $this->invokeMethod($this->controller, 'inputsValidate', array(true, true, false, 10, 20, 30));
        $result1 = $this->invokeMethod($this->controller, 'calculate_sp1', array());

        $this->invokeMethod($this->controller, 'inputsValidate', array(true, true, true, 10, 20, 30));
        $result2 = $this->invokeMethod($this->controller, 'calculate_sp1', array());

        $this->invokeMethod($this->controller, 'inputsValidate', array(false, true, true, 10, 20, 30));
        $result3 = $this->invokeMethod($this->controller, 'calculate_sp1', array());

        $this->invokeMethod($this->controller, 'inputsValidate', array(false, true, false, 10, 20, 30));
        $result4 = $this->invokeMethod($this->controller, 'calculate_sp1', array());

        $this->assertEquals(['X' => 'S', 'Y' => 12], $result1);
        $this->assertEquals(['X' => 'R', 'Y' => 22], $result2);
        $this->assertEquals(['X' => 'T', 'Y' => 8], $result3);
        $this->assertEquals(['X' => 'other', 'Y' => 'error'], $result4);
    }

    public function test_of_calculating_the_sp2()
    {
        $this->invokeMethod($this->controller, 'inputsValidate', array(true, true, false, 10, 20, 30));
        $result1 = $this->invokeMethod($this->controller, 'calculate_sp2', array());

        $this->invokeMethod($this->controller, 'inputsValidate', array(true, true, true, 10, 20, 30));
        $result2 = $this->invokeMethod($this->controller, 'calculate_sp2', array());

        $this->invokeMethod($this->controller, 'inputsValidate', array(false, true, true, 10, 20, 30));
        $result3 = $this->invokeMethod($this->controller, 'calculate_sp2', array());

        $this->invokeMethod($this->controller, 'inputsValidate', array(true, false, true, 10, 20, 30));
        $result4 = $this->invokeMethod($this->controller, 'calculate_sp2', array());

        $this->invokeMethod($this->controller, 'inputsValidate', array(false, true, false, 10, 20, 30));
        $result5 = $this->invokeMethod($this->controller, 'calculate_sp2', array());

        $this->assertEquals(['X' => 'T', 'Y' => 8], $result1);
        $this->assertEquals(['X' => 'R', 'Y' => 9], $result2);
        $this->assertEquals(['X' => 'T', 'Y' => 8], $result3);
        $this->assertEquals(['X' => 'S', 'Y' => 42], $result4);
        $this->assertEquals(['X' => 'other', 'Y' => 'error'], $result5);
    }

    public function test_validanting_the_get_base_call()
    {
        $result1 = $this->controller->getBase(true, true, true, 10, 20, 30);
        $result2 = $this->controller->getBase(null, false, false, 10, 20, 30);
        $result3 = $this->controller->getBase(true, true, true, 10, 20, null);

        $this->assertEquals('{"X":"R","Y":9}', $result1);
        $this->assertEquals('Invalid input given.', $result2);
        $this->assertEquals('Invalid input given.', $result3);
    }

    public function test_validanting_the_get_sp1_call()
    {
        $result1 = $this->controller->getSP1(true, true, true, 10, 20, 30);
        $result2 = $this->controller->getSP1(null, false, false, 10, 20, 30);
        $result3 = $this->controller->getSP1(true, true, true, 10, 20, null);

        $this->assertEquals('{"X":"R","Y":22}', $result1);
        $this->assertEquals('Invalid input given.', $result2);
        $this->assertEquals('Invalid input given.', $result3);
    }

    public function test_validanting_the_get_sp2_call()
    {
        $result1 = $this->controller->getSP2(true, true, true, 10, 20, 30);
        $result2 = $this->controller->getSP2(null, false, false, 10, 20, 30);
        $result3 = $this->controller->getSP2(true, true, true, 10, 20, null);

        $this->assertEquals('{"X":"R","Y":9}', $result1);
        $this->assertEquals('Invalid input given.', $result2);
        $this->assertEquals('Invalid input given.', $result3);
    }
}