<?php

namespace Tests;

use PHPUnit_Framework_TestCase;
use App\TTFApp;

class TTFAppTest extends PHPUnit_Framework_TestCase
{
    protected $app;

    public function setUp()
    {
        parent::setUp();

        $this->app = new TTFApp();
    }

    public function test_get_app_version()
    {
        $result = $this->app->version();
        $expected = "TTF Assigment Version 1.0.0.";

        $this->assertEquals($expected, $result);
    }

    public function test_of_getting_app_to_start()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $_SERVER['REQUEST_URI'] = '/';

        $result = $this->app->start();
        $expected = "Initial Page";

        $this->assertEquals($expected, $result);
    }

}